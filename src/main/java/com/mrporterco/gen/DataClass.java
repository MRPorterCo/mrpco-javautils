package com.mrporterco.gen;

public interface DataClass {

    int length();
    String getData();
    
}
