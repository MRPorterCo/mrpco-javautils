package com.mrporterco.collections.spatial;

public interface SpatialItem {
	
	long getX();
	long getY();
	long getZ();

}
