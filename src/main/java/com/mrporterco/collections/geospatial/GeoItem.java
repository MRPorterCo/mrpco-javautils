package com.mrporterco.collections.geospatial;

public interface GeoItem {
	
	double getLatitude();
	double getLongitude();
	double getAltitude();

}
