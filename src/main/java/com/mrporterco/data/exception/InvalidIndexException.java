package com.mrporterco.data.exception;

public class InvalidIndexException extends HashBlobException {

	public InvalidIndexException(String msg) {
		super(msg);
	}

}
