package com.mrporterco.data.exception;

public class CorruptedDataException extends HashBlobException {

    public CorruptedDataException(String msg) {
        super(msg);
    }

}
