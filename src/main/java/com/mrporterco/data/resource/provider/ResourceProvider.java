package com.mrporterco.data.resource.provider;

import com.mrporterco.data.exception.ResourceException;


public interface ResourceProvider<TValue> {
	
	boolean exists() throws ResourceException;
	TValue provide() throws ResourceException; 
	
}
