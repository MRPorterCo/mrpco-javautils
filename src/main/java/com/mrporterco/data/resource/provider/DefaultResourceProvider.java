package com.mrporterco.data.resource.provider;

import com.mrporterco.data.exception.ResourceException;

public abstract class DefaultResourceProvider<Value> implements ResourceProvider<Value> {

    @Override
    public boolean exists() throws ResourceException {
        return provide() != null;
    }
    
}
