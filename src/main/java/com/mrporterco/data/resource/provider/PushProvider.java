package com.mrporterco.data.resource.provider;

import com.mrporterco.data.exception.ResourceException;

public interface PushProvider<Value> extends ResourceProvider<Value> {

    void push(Value value) throws ResourceException;
    
}
