package com.mrporterco.data.resource.cache;

import com.mrporterco.data.exception.ResourceException;
import com.mrporterco.data.resource.loader.ResourceLoader;

public interface Cache<TKey, TValue> extends ResourceLoader<TKey, TValue> {

    /**
     * @throws ResourceException 
     * 
     */
	void clear() throws ResourceException;
	
	/**
	 * 
	 * @param key
	 */
	void remove(TKey key) throws ResourceException;
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	void put(TKey key, TValue value) throws ResourceException;
	
}
