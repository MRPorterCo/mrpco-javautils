package com.mrporterco.util;

import java.util.concurrent.Executor;

import com.mrporterco.concurrency.Callback;
import com.mrporterco.concurrency.CallbackFinalizer;
import com.mrporterco.concurrency.WeakCallback;
import com.mrporterco.data.exception.ResourceException;
import com.mrporterco.data.resource.loader.ParallelResourceLoader;
import com.mrporterco.data.resource.loader.ResourceEvent;

public class ResourceLoaderUtil {
    
    public static <Key, Value> void callParallelAndRun(
        final Executor runner, 
        final ParallelResourceLoader<Key, Value> loader, 
        final CallbackFinalizer finalizer,
        final Callback<Object, ResourceEvent<Value>> callback, 
        final Key key
    ) throws ResourceException {
        
        loader.getParallel(
            new WeakCallback<Object, ResourceEvent<Value>>(
                new Callback<Object, ResourceEvent<Value>>() {

                    @Override
                    public Object call(final ResourceEvent<Value> value) {
                        
                        runner.execute(
                            new Runnable() {

                                @Override
                                public void run() {
                                    callback.call(value);
                                }
                            }
                        );
                        
                        return null;
                        
                    }
                },
                false,
                finalizer
            ),
            key
        );
        
        
    }
    
    

}
