package com.mrporterco.javacommon.test.cache;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import com.mrporterco.data.exception.ResourceException;
import com.mrporterco.data.resource.cache.Cache;
import com.mrporterco.data.resource.cache.ResourceLoaderCache;
import com.mrporterco.data.resource.loader.DefaultResourceLoader;

public class ResourceLoaderCacheTest {
	
    @Test
    public void test() {
    	
    	try{
    		
			final Cache<String, String> cache = 
				new ResourceLoaderCache<String, String>(
					new DefaultResourceLoader<String, String>() {
	
						@Override
						public String get(String key) throws ResourceException {
			                return key;
						}
					}
				);
	        
            cache.put("key", "test");
            TestCase.assertEquals(cache.get("key"), "key");
            cache.remove("key");
            TestCase.assertEquals(cache.exists("key"), true);
            cache.clear();
            
            {
            	
        	final List<String> keys = new ArrayList<String>();
            
            keys.add("test1");
            keys.add("test2");
            
            final List<String> results = cache.getAll(keys);
            	
            TestCase.assertEquals(results.get(0), cache.get("test1"));
            TestCase.assertEquals(results.get(1), cache.get("test2"));
            
            }
            
    	}
    	
        catch(ResourceException e) {
            TestCase.fail("failed");
        }
        
    }
    
}
